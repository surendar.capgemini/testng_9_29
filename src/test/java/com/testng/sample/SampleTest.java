package com.testng.sample;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;




public class SampleTest {

	private WebDriver fireFoxDriver, chromeDriver, ieDriver;
	final static Logger logger = Logger.getLogger(SampleTest.class);

	@BeforeMethod
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\surmeesa\\GradedLab\\TestNGSample\\geckodriver.exe");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\surmeesa\\GradedLab\\TestNGSample\\chromedriver.exe");
		System.setProperty("webdriver.ie.driver", "C:\\Users\\surmeesa\\GradedLab\\TestNGSample\\IEDriverServer.exe");
	}

	@Test
	public void testopenFireFoxdriver() {
		
		try {
			fireFoxDriver = new FirefoxDriver();
			fireFoxDriver.get("http://www.amazon.com");
			
			WebElement todaysDealsLink = fireFoxDriver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[2]"));
			todaysDealsLink.click();
		}
		catch (Exception e) {
			logger.info("Firefox browser opened");
		}
	}

	@Test
	public void testChromeDriver() {
		try {
			chromeDriver = new ChromeDriver();
			chromeDriver.get("http://www.amazon.com");
			
			WebElement searchBox = chromeDriver.findElement(By.id("twotabsearchtextbox"));
			searchBox.sendKeys("iphone 8");
			WebElement submitButton = chromeDriver.findElement(By.xpath("//*[@id=\"nav-search\"]/form/div[2]/div/input"));
			submitButton.click();
		}
		catch (Exception e) {
			logger.info("Firefox browser opened");
		}
	}
	
	@Test
	public void testOpenIEDriver() {
			ieDriver = new InternetExplorerDriver();
			ieDriver.get("http://www.amazon.com");
	}
	
	@AfterMethod
    public void afterMethod() {
		fireFoxDriver.close();
		chromeDriver.close();
		ieDriver.close();
    }
}
